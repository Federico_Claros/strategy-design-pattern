package test_2;

/**
 * Abstract Class ACard
 * @version 1.0
 * Mold from which every single card emanates.
 */
public abstract class ACard implements IDiscardBehavior {
    private final EColor color;

    public ACard (EColor color) {
        this.color = color;
    }

    public EColor getColor(){
        return this.color;
    }

    @Override
    public String toString(){
        return this.getColor() +" }";
    }
}
