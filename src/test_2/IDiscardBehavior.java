package test_2;

public interface IDiscardBehavior {
    public boolean discard();
}
