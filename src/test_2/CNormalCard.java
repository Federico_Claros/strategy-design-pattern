package test_2;

public class CNormalCard extends ACard {
    private final int number;

    public CNormalCard(int number, EColor color){
        super(color);
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString(){
        return "{ "+this.getNumber()+", "+super.toString();
    }

    @Override
    public boolean discard() {
        boolean discarded = false;
        ACard topCard = CDiscardPile.getInstance().getTopCard();
        // If both cards share the same color
        if(this.getColor() == topCard.getColor()) {
            discarded = true;
        }
        // If top card is a Normal Card and equal in number to this card.
        if(topCard.getClass().isAssignableFrom(this.getClass())){
            if(this.getNumber() == ((CNormalCard) topCard).getNumber()){
                discarded = true;
            }
        }
        // If discarded is true, put this card on top of the pile.
        if(discarded){
            CDiscardPile.getInstance().putCardOnTop(this);
        }
        return discarded;
    }
}
