package test_2;

public class CSpecialCard extends ACard {
    private final ESpecial type;

    public CSpecialCard(ESpecial type, EColor color) {
        super(color);
        this.type = type;
    }

    public ESpecial getType() {
        return type;
    }

    @Override
    public String toString(){
        return "{ "+this.getType()+", "+super.toString();
    }

    @Override
    public boolean discard() {
        boolean discarded = false;
        ACard topCard = CDiscardPile.getInstance().getTopCard();
        // If both cards are the same color or this card is multicolor.
        if(this.getColor() == topCard.getColor() || this.getColor().equals(EColor.MULTICOLOR)){
            discarded = true;
        }
        // If top card is special, check if both cards are the same type.
        if(topCard.getClass().isAssignableFrom(this.getClass())) {
            if(this.getType() == ((CSpecialCard) topCard).getType()){
                discarded = true;
            }
        }
        // If discarded true, put this card on top of the pile.
        if(discarded){
            CDiscardPile.getInstance().putCardOnTop(this);
        }

        return discarded;
    }
}
