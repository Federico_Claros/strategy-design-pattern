package test_2;

import java.util.ArrayList;

/**
 * Test 2
 * @version 1.0
 * Reach: test a possible implementation of Strategy Design Pattern, in the card game Uno's discard logic.
 *      + Those cards with same color can be put on top of the pile.
 *      + Those cards with same number/type can be put on top of the pile.
 *      + A card with multicolor can be place on top of the pile ever.
 * Limitations:
 *      + There is no Factory Design Pattern to handle legal Uno's card creation.
 *      + There is no Handler/Layer which ensure Special Cards' effect take place in the game.
 * Justification:
 *      The Strategy Design Pattern can be applied in a Uno game when discarding card on top of the pile.
 *          The discard Algorithm is slightly different when discarding a Normal card from a Special one.
 *          The first, has only a number and a simple color = { GREEN, BLUE, RED, YELLOW } so it must check,
 *          if either a number or a simple color is equal to the top card from the pile.
 *          Whereas a special card which is capable of being multicolored i.e. +4 or change color, must check,
 *          if either a type or a simple color is equal to the top card from the pile,
 *          plus! should the special card to be discarded be multicolored, must be discarded regardless of top card's color.
 */
public class Test {
    public static void main(String[] arg) {
        ACard card = new CNormalCard(1,EColor.BLUE);
        ArrayList<ACard> cards = new ArrayList<>();

        cards.add(new CNormalCard(1,EColor.GREEN));
        cards.add(new CNormalCard(2,EColor.RED));
        cards.add(new CNormalCard(2,EColor.GREEN));
        cards.add(new CNormalCard(2,EColor.RED));
        cards.add(new CSpecialCard(ESpecial.CHANGE_COLOR, EColor.MULTICOLOR));
        /*
        * Note: This constructors must be handled by another Design Pattern for creation, so legal cards are created.
        */
        System.out.println("[MSG] Test: Uno discard card logic.");

        CDiscardPile.getInstance().putCardOnTop(card);

        System.out.println("\t[MSG] Top Card is: "+CDiscardPile.getInstance().getTopCard());
        for (ACard c: cards) {
            if(c.discard()){
                System.out.println("\t[MSG] Successfully discarded card!!");
                // If discarded card was a Special:
                //      Another Layer of code must handle the User interaction to change this card's color
                //          And/Or to make the effect take place, i.e. Take +2, Change color, Cancel Turn, etc.
            }
            else {
                System.out.println("\t[ERR] The following card: "+ c +" cannot be discarded!!");
            }
            System.out.println("\t[MSG] Top Card is: "+CDiscardPile.getInstance().getTopCard());
        }
    }
}
