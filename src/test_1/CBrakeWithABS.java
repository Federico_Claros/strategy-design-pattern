package test_1;

public class CBrakeWithABS implements IBrakeBehaviour {
    @Override
    public void useBreak() {
        System.out.println("\t[MSG] Brake with ABS applied!");
    }
}
