package test_1;

class Test {
    public static void main(String[] arg){
        ACar auto1 = new CSenda();
        ACar auto2 = new CRenault12();

        auto1.applyBrake();
        auto2.applyBrake();

        auto2.setBrakeBehaviour(new CBrakeWithABS());
        auto2.applyBrake();
    }
}