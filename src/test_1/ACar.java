package test_1;

public abstract class ACar {
    private IBrakeBehaviour brakeBehaviour;

    public ACar(IBrakeBehaviour brakeBehaviour) {
        this.setBrakeBehaviour(brakeBehaviour);
    }

    public void applyBrake() {
        brakeBehaviour.useBreak();
    }

    public void setBrakeBehaviour( IBrakeBehaviour brakeBehaviour){
        this.brakeBehaviour = brakeBehaviour;
    }

}
