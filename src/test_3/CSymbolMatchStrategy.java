package test_3;

public class CSymbolMatchStrategy implements IPutCardOnTopPileStrategy {

    @Override
    public boolean canPlaceCardOnTopPile(ACard topCard, ACard card) {
        boolean discarded = false;
        if(topCard.getTypeOfCard() == card.getTypeOfCard() && card.getTypeOfCard().equals(ETypeOfCard.SPECIAL)){
            if(((CSpecialCard) topCard).getSymbol() == ((CSpecialCard) card).getSymbol()){
                discarded = true;
            }
        }
        return discarded;
    }
}
