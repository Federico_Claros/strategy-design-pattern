package test_3;

public class CNumberMatchStrategy implements IPutCardOnTopPileStrategy{
    @Override
    public boolean canPlaceCardOnTopPile(ACard topCard, ACard card) {
        boolean discarded = false;
        if(topCard.getTypeOfCard() == card.getTypeOfCard() && card.getTypeOfCard().equals(ETypeOfCard.NORMAL)){
            if(((CNormalCard) topCard).getNumber() == ((CNormalCard) card).getNumber()){
                discarded = true;
            }
        }
        return discarded;
    }
}
