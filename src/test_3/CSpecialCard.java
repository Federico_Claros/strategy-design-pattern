package test_3;

public class CSpecialCard extends ACard{
    private final ESpecial symbol;
    public CSpecialCard(ESpecial symbol, EColor color) {
        super(color, ETypeOfCard.SPECIAL, new CColorAndSymbolMatchStrategy());
        this.symbol = symbol;
    }

    public ESpecial getSymbol() {
        return this.symbol;
    }

    @Override
    public String toString(){
        return "{ "+this.getSymbol()+", "+super.toString();
    }
}
