package test_3;

public class CNormalCard extends ACard {
    private final int number;
    public CNormalCard(int number, EColor color) {
        super(color,ETypeOfCard.NORMAL, new CColorAndNumberMatchStrategy());
        this.number = number;
    }

    public int getNumber(){
        return this.number;
    }

    @Override
    public String toString(){
        return "{ "+this.getNumber()+", "+super.toString();
    }
}
