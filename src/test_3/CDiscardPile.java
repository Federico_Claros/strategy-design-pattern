package test_3;

import java.util.Stack;

/**
 * Singleton Class CDiscardPile
 * @version 1.0
 * This hold the top discarded card in a game of Uno.
 */
public class CDiscardPile {
    private static final CDiscardPile instance = new CDiscardPile();

    private CDiscardPile(){}

    public static CDiscardPile getInstance() {
        return instance;
    }

    private final Stack<ACard> stack = new Stack<>();

    public void putCardOnTop(ACard card){
        this.stack.push(card);
    }

    public ACard getTopCard(){
        ACard aux = this.stack.pop(); // pop top card from pile
        this.stack.push(aux); // put back card to pile
        return aux; // return copy
    }
}
