package test_3;

/**
 * Abstract Class ACard
 * @version 1.0
 * Mold from which every single card emanates.
 */
public abstract class ACard {
    private final EColor color;
    private final ETypeOfCard typeOfCard;
    private IPutCardOnTopPileStrategy strategy;

    public ACard (EColor color, ETypeOfCard typeOfCard, IPutCardOnTopPileStrategy strategy) {
        this.color = color;
        this.typeOfCard = typeOfCard;
        this.strategy = strategy;
    }

    public EColor getColor(){
        return this.color;
    }
    public ETypeOfCard getTypeOfCard() { return this.typeOfCard; }

    public boolean putOnTopOfPile(){
        return this.strategy.canPlaceCardOnTopPile(CDiscardPile.getInstance().getTopCard(), this);
    }

    @Override
    public String toString(){
        return this.getColor() +" }";
    }
}
