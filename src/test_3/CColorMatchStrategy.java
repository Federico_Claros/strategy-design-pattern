package test_3;

public class CColorMatchStrategy implements IPutCardOnTopPileStrategy {

    @Override
    public boolean canPlaceCardOnTopPile(ACard topCard, ACard card) {
        boolean discarded = false;
        if(topCard.getColor() == card.getColor()){
            discarded = true;
        }
        if(card.getColor().equals(EColor.MULTICOLOR)){
            discarded = true;
        }
        return discarded;
    }
}
