package test_3;

public class CColorAndSymbolMatchStrategy implements IPutCardOnTopPileStrategy {
    private final IPutCardOnTopPileStrategy symbolStrat = new CSymbolMatchStrategy();
    private final IPutCardOnTopPileStrategy colorStrat = new CColorMatchStrategy();

    @Override
    public boolean canPlaceCardOnTopPile(ACard topCard, ACard card) {
        boolean discarded = false;
        if(symbolStrat.canPlaceCardOnTopPile(topCard, card) || colorStrat.canPlaceCardOnTopPile(topCard, card)){
            discarded = true;
        }
        return discarded;
    }
}
