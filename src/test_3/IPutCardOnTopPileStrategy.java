package test_3;

public interface IPutCardOnTopPileStrategy {
    boolean canPlaceCardOnTopPile(ACard topCard, ACard card);
}
