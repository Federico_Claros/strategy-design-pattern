package test_3;

public class CColorAndNumberMatchStrategy implements IPutCardOnTopPileStrategy {
    private final IPutCardOnTopPileStrategy numberStrat = new CNumberMatchStrategy();
    private final IPutCardOnTopPileStrategy colorStrat = new CColorMatchStrategy();
    @Override

    public boolean canPlaceCardOnTopPile(ACard topCard, ACard card) {
        boolean discarded = false;
        if(numberStrat.canPlaceCardOnTopPile(topCard,card) || colorStrat.canPlaceCardOnTopPile(topCard, card)){
            discarded = true;
        }
        return discarded;
    }
}
